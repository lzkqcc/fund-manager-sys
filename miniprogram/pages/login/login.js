const WXuserInfo = require("./wxuserInfo.js");
// const {
//   $Toast
// } = require('../../static/iview/base/index');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    usedLogin: false, //缓存判别是否登录过
    wxUserInfo: null, //小程序登录信息
    loginType: "oneClick", //一键登录 
    input: {},
    userName: '',
    accountValue: '',
    passwordValue: "",
    userInfo: {},
    userImage: '',
    globalData: {},
    phoneNumber: 0
  },


  /**
   * 1. 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let guid = "v2guid";
    var that = this;
    wx.getStorage({
      key: guid,
      success: function (res) {

        that.queryLoginStateByCache();
      },
      fail: function (res) {
        wx.removeStorageSync("wxUserInfo");
        console.log("刷新用户")
        wx.setStorage({
          key: 'v2guid',
          data: true
        });
        that.queryLoginStateByCache();
      }
    });

    // 获取用户信息
    // this.getUserInfo();

    // 获取手机号方法
    // this.wxLogin();

  },
  getPhoneNumber(cloudID) {
    // wx.cloud.callFunction({
    //   name: 'getOpenid',
    //   data: {
    //     weRunData: cloudID, // 这个 CloudID 值到云函数端会被替换
    //   complete: res => { 
    //     let phone = res.result.weRunData.data.phoneNumber;
    //     this.setData({
    //       phoneNumber:phone
    //     })



    //   }
    wx.cloud.callFunction({
      name: 'getOpenid',
      data: {
        weRunData: wx.cloud.CloudID(cloudID)
      }, // 这个 CloudID 值到云函数端会被替换
      complete: res => {
        console.log('云函数获取到的openid: ', res)
        let phone = res.result.weRunData.data.phoneNumber;
        // debugger
        //得到openid，从云端读取数据，若存在该用户，则登录，若不存在，则添加
        // const db = wx.cloud.database()
        this.setData({
          phoneNumber: phone
        })


      }
    })

  },

  // 获取用户信息
  getUserInfo() {
    // var shareTickets = res.shareTickets[0];

    wx.getUserInfo({
      complete: (res) => {
        let UserInfo = res.userInfo;
        this.setData({
          userName: UserInfo.nickName,
          userImage: UserInfo.avatarUrl
        })
        debugger
        // 缓存用户图片信息
        wx.setStorageSync("UserInfo", UserInfo)

      },
    });

  },
  /**
   * 1.1 获取缓存中是否存在已经登录过得缓存信息，key wxUserInfo
   */
  queryLoginStateByCache() {
    var that = this;
    wx.getStorage({
      key: 'wxUserInfo',
      success: function (res) {
        that.existLoginInfo(res.data);
      },
      fail: function (res) {
        that.firstTimeLogin();
      }
    })
  },
  /**
   * 1.2 缓存存在登录信息，直接登录
   */
  existLoginInfo(data) {
    console.log(" 存在登录信息");
    //设置页面开关usedLogin-->true
    this.setData({
      usedLogin: true
    })

    let wxUserInfo = new WXuserInfo(data); //读取key值为myData的缓存数据
    this.setData({
      wxUserInfo: wxUserInfo
    })

    // 跳转至 用户内部页面
    this.successLogin(wxUserInfo);
  },

  /**
   * 1.3 第一次登录，转向注册页面
   */
  firstTimeLogin() {
    this.setData({
      usedLogin: false
    })
    console.log(" 无缓存权限数据");
  },
  /**
   * 页面修改
   */
  changeLoginTypeClick({
    detail
  }) {
    this.setData({
      loginType: detail.key
    });
  },
  //点击一键登录
  oneClickLoginBtnClick(e) {
    if (e.detail.errMsg == 'getPhoneNumber:fail user deny') { //用户点击拒绝
      console.log("拒绝");
    } else { //授权通过执行跳转
      console.log("同意");
      //获取手机号
      console.log(e.detail)
      console.log(e.detail.cloudID);
      //获取手机号



      //获取openid
      let that = this;
      wx.cloud.callFunction({
        name: 'getOpenid',
        data: {
          weRunData: wx.cloud.CloudID(e.detail.cloudID)
        }, // 这个 CloudID 值到云函数端会被替换
        complete: res => {
          console.log('云函数获取到的openid: ', res)
          // debugger
          let openId = res.result.userInfo.openId;
          let phone = res.result.weRunData.data.phoneNumber;
          //得到openid，从云端读取数据，若存在该用户，则登录，若不存在，则添加
          const db = wx.cloud.database()

          db.collection('wxUserInfo').where({
            phone: phone
          }).get({
            success: res => {
              if (res.data.length == 0) {
                //不存在用户
                //进入注册页面
                that.failLogin(openId, phone);

              } else {
                //存在用户
                that.successLogin(res.data[0]);
              }
            },
          })
        }
      })
    };


  },
  failLogin(openId, phone) {
    let that = this;
    // wx.showToast({
    //   title: "用户初次登录请完善信息",
    //   icon: 'none',
    //   duration: 2000,
    // })
    setTimeout(function () {
      wx.reLaunch({
        url: '../wxRegister/wxRegister?openId=' + openId + '&phone=' + phone
      })
    }, 2000)
  },
  //将登录信息报错在缓存和页面中，进入下一个页面
  successLogin(data) {
    let wxUserInfo = new WXuserInfo(data);

    this.setData({
      wxUserInfo: wxUserInfo
    });

    //存在信息，成功登录
    wx.setStorage({
      key: 'wxUserInfo',
      data: wxUserInfo
    });

    let that = this;
    // wx.showToast({
    //   title: "成功登录",
    //   icon: 'success',
    //   duration: 2000,
    // })

    // debugger



    setTimeout(function () {
      // debugger
      //读取到缓存数据，根据角色进入不同页面
      let wxUserInfo = that.data.wxUserInfo;
      let wxUserInfoStr = JSON.stringify(wxUserInfo);
      switch (wxUserInfo.privilege.roleCode) {
        case "dataInput":
          wx.reLaunch({
            url: '../index-operation/index-operation?wxUserInfo=' + wxUserInfoStr
          });
          break;
        case "dataViewSection":
          wx.reLaunch({
            url: '../index/index?wxUserInfo=' + wxUserInfoStr
          });
          break;
        case "dataViewProject":
          wx.reLaunch({
            //url: '../index-project/index-project?wxUserInfo=' + wxUserInfoStr
            url: '../../subpages/DataViewProject/DataViewProject?wxUserInfo=' + wxUserInfoStr

          });
          break;
        default: break;
      }
    }, 2000)
  },
  changeLoginMode(e) {
    // debugger
    console.log(e.currentTarget.dataset.mode)
    this.setData({
      loginType: e.currentTarget.dataset.mode
    })
  },
  changeToUserPassword(e) {
    this.setData({
      loginType: "userPassword"
    });
  },
  /**
   * 账户密码登录
   */
  userPasswordBtnClick() {
    // debugger
    //查询数据库，判断是否存在该用户
    const db = wx.cloud.database()

    db.collection('wxUserInfo').where({
      userId: this.data.input.userId
    }).get({
      success: res => {
        if (res.data.length == 0) {
          console.log("用户不存在", res);
          //密码错误
          // wx.showToast({
          //   title: "不存在该登录账户！",
          //   icon: 'none',
          //   duration: 2000,
          // })
        } else
          if (res.data[0].userPassword == this.data.input.userPassword) {
            //成功登录
            this.successLogin(res.data[0]);
          } else {
            //密码错误
            // wx.showToast({
            //   title: "密码错误，请重新输入！",
            //   icon: 'none',
            //   duration: 2000,
            // })
          }
      },
    })
  },
  /**
   * 用户账号输入
   */
  bindUserInput(e) {
    this.data.input.userId = e.detail.value;
    this.setData({
      input: this.data.input
    });
  },
  // 清空用户输入内容
  clearAccountValue() {
    // debugger
    this.setData({
      accountValue: ""
    })
  },
  /**
   * 用户密码输入
   */
  bindPasswordInput(e) {
    this.data.input.userPassword = e.detail.value;
    this.setData({
      input: this.data.input
    });
  },
  // 清空用户密码输入
  clearPasswordValue() {
    // debugger
    this.setData({
      passwordValue: ''
    })
  }
})