//微信权限类
function wxprivilege(options) {
  this.prjCode = options.prjCode;        //项目代号
  this.roleCode = options.roleCode;      //角色代号，目前考虑单个项目内的角色
  this.sectionCode = options.sectionCode; //标段代号
}


module.exports = wxprivilege;