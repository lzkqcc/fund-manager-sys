const wxprivilege = require('./wxprivilege.js')  //

//微信用户信息类
function wxuserInfo (options){
  this.userId = options.userId;      //微信账户，默认一键登录为openId
  this.userName  = options.userName;  //小程序内名称，默认为微信昵称
  this.userPassword = options.userPassword;
  this.phone = options.phone;
 
  if (options.privilege!=null){
    this.privilege = new wxprivilege(options.privilege);             //权限信息，
  }else{
    this.privilege = null;
  }
 
}


module.exports = wxuserInfo;