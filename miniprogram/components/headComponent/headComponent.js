// components/headComponent/headComponent.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    // userInfo:{
    //   type:Object,
    //   default:{}
    // },
    headerName: {
      type: String,
      default: ""
    },
    dateTimeIsShow: {
      type: Boolean,
      default: false
    },
    slotName: {
      type: String,
      default: ''
    },
    calendar: {
      type: Object,
      default: {}
    },

    proj: {
      type: Array,
      default: []
    },
    // hiddenDateModals:{
    //   type:Boolean,
    //   default:false
    // },
    needImageView: {
      type: Boolean,
      default: false
    },
    needRiskView: {
      type: Boolean,
      default: false
    },
  },
  data: {
    wxUserInfo: null,
    userInfo: {},
    // proj: [],
    testProj: [],
    avatarUrl: './img/userImage.png',
    progressBackGroundArr: ["#E1F3D8", "#E1F3D8", "#E1F3D8"],
    progressTextColorArr: ["#67C23A", "#67C23A", "#67C23A"],
    progressBorderColorArr: ["#DCDFE6", "#DCDFE6", "#DCDFE6"],
    isShow: false,
    array: ['注销'],
    index: 0,
    currentTab: 0,
    nowDay: null,


    // calendar:{},


    // 底部弹出框
    hideModal: true, //模态框的状态  true-隐藏  false-显示
    animationData: {},//
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
